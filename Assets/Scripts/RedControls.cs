
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RedControls : MonoBehaviour
{
    Rigidbody2D rb2d;
    public float speed = 5f;
    [SerializeField]
    private float flapforce = 20f;
    bool isDead;
    void Start()
    {
        Time.timeScale = 0;

        rb2d = GetComponent<Rigidbody2D>();
        
        rb2d.velocity = Vector2.right * speed * Time.deltaTime;

    }
 
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && !isDead)
        {

            rb2d.velocity = Vector2.right * speed * Time.deltaTime;

            rb2d.AddForce(Vector2.up * flapforce);
        }
    }

    public GameObject ReplayButton;
    void OnCollisionEnter2D(Collision2D col)
    {
        isDead = true;
        rb2d.velocity = Vector2.zero;

        GetComponent<Animator>().SetBool("isDead", true);

        ReplayButton.SetActive(true);

    }

    public void Replay()
    {
        SceneManager.LoadScene(1);
    }

    public void UnFreeze()
    {
        Time.timeScale = 1;
    }

    public Text scoretext;
    int score = 0;
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Score")
        {
            score++;
            Debug.Log(score);
            scoretext.text = score.ToString();
        }
    }
}
